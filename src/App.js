import './App.css';
import Sidebar from "./components/Sidebar/Sidebar";
import Router from "./router/Router";
import {useIsMobile} from "./components/common/useIsMobile";

function App() {
    return (
        !useIsMobile() ?
        <div className="flex bg-[#556B80] min-h-screen app-container">
            <div className="col-auto sticky top-0 h-screen">
                <Sidebar/>
            </div>
            <div className="p-5 sticky w-screen">
                <Router/>
            </div>
        </div>
        :
        <div className="flex flex-col bg-[#556B80] min-h-screen app-container">
            <div className="p-5 flex-grow">
                <Router/>
            </div>
            <div className="sticky bottom-0">
                <Sidebar/>
            </div>
        </div>
  )
}

export default App;
