import {useEffect, useState} from "react";

export function useIsMobile() {
    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        const mobileMediaQuery = window.matchMedia('(max-width: 767px)');

        const handleMobileChange = (event) => {
            setIsMobile(event.matches);
        };

        mobileMediaQuery.addEventListener('change', handleMobileChange);
        setIsMobile(mobileMediaQuery.matches);

        return () => {
            mobileMediaQuery.removeEventListener('change', handleMobileChange);
        };
    }, [])

    return isMobile
}

