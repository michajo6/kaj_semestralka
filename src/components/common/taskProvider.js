export const getStorageTasks = () => {
    const tasks = localStorage.getItem('tasks');
    return tasks ? JSON.parse(tasks) : []
}

export const setStorageTasks = (tasks) => {
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

export const addTask = (newTask) => {
    const prevTasks = getStorageTasks()
    newTask.id = prevTasks.reduce((max, task) => (task.id > max ? task.id : max), 0) + 1;

    setStorageTasks([...prevTasks, newTask]);
};

export const removeTask = (taskId) => {
    setStorageTasks(getStorageTasks().filter(task => task.id !== taskId))
};

export const updateTasks = (updatedTasks, type) => {
    const newTasks = type === "all" ? updatedTasks : getStorageTasks().filter(task => task.type !== type).concat(updatedTasks)

    setStorageTasks(newTasks)
}

export const updateTask = (updatedTask) => {
    const updatedTasks = getStorageTasks().map(task =>
        task.id === updatedTask.id ? { ...task, ...updatedTask } : task
    );

    setStorageTasks(updatedTasks)
};