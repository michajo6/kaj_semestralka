import React, { useEffect, useState } from 'react';
import { Sidebar as ReactSidebar, Menu, MenuItem } from 'react-pro-sidebar';
import { Link, useLocation } from "react-router-dom";
import logo from "../../images/logo.png";
import activeLogo from "../../images/activeLogo.png";
import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import "./sidebar.css";
import {FaHome} from "@react-icons/all-files/fa/FaHome";
import {FaTasks} from "@react-icons/all-files/fa/FaTasks";
import {FaClock} from "@react-icons/all-files/fa/FaClock";
import {FaCheck} from "@react-icons/all-files/fa/FaCheck";
import {FaChartBar} from "@react-icons/all-files/fa/FaChartBar";
import {FaHourglass} from "@react-icons/all-files/fa/FaHourglass";
import {useIsMobile} from "../common/useIsMobile";
import AddTask from "../Tasks/add/AddTask";
import {FaPlus} from "@react-icons/all-files/fa/FaPlus";
import AddTaskModal from "../Tasks/add/AddTaskModal";
import {FaSpinner} from "@react-icons/all-files/fa/FaSpinner";

export const routes = [
    {title: "Home", to: "/", icon: <FaHome />},
    {title: "All", to: "/all", icon: <FaTasks />},
    {title: "To do", to: "/to-do", icon: <FaClock />},
    {title: "In Progress", to: "/in-progress", icon: <FaSpinner />},
    {title: "Done", to: "/done", icon: <FaCheck />},
]

const Sidebar = () => {
    const location = useLocation();
    const [activeItem, setActiveItem] = useState("/" + location.pathname.split("/")[1]);
    const [isOpen, setIsOpen] = useState(false)


    useEffect(() => {
        setActiveItem("/" + location.pathname.split("/")[1]);
    }, [location.pathname]);


    if (useIsMobile()) {
        return (
            <Box sx={{ width: '100%' }} key='bottom-nav-box'>
                <BottomNavigation
                    key='bottom-nav'
                    sx={{
                        backgroundColor: '#00c1e8',
                        color: "black",
                        '& .Mui-selected': {
                            color: "black !important",
                            backgroundColor: '#008BB0 !important',
                        },
                    }}
                    value={
                        routes.findIndex(route => route.to === activeItem)
                    }
                >
                    {routes.map((value, index) => (
                        <BottomNavigationAction
                            label={value.title}
                            icon={value.icon}
                            component={Link}
                            to={value.to}
                            key={`bottom-menu-item-${value.to}`}
                            sx={{ padding: "10px 0 0 0", margin: 0 }}
                        />
                    ))}
                </BottomNavigation>
            </Box>
        );
    }

    return (
        <div className="relative" key='sidebar-container'>
            <ReactSidebar className="min-h-screen sidebar" key='sidebar'>
                <Link to="/" key='home-logo-link'>
                    <img
                        key='home-logo'
                        src={"/" === activeItem ? activeLogo : logo} alt="Logo"
                        className="p-10 bg-[#36525B]"
                    />
                </Link>
                <Menu
                    key='sidebar-menu-div'
                    menuItemStyles={{
                        button: () => {
                            return {
                                "&:hover": {
                                    backgroundColor: "#008BB0",
                                    transition: "background-color 0.5s"
                                },
                            };
                        },
                    }}
                >
                    {routes.map((item) => (
                        <MenuItem
                            key={`menu-item-${item.to}`}
                            className={`menu-item ${item.to === activeItem ? "active" : ""}`}
                            icon={item.icon}
                            component={<Link to={item.to} />}
                        >
                            {item.title}
                        </MenuItem>
                    ))}
                </Menu>
            </ReactSidebar>
            <div className="absolute bottom-5 left-1/2 transform -translate-x-1/2 text-white truncate" key='app-signature'>App by Josef Michálek</div>
        </div>
    );
};

export default Sidebar;
