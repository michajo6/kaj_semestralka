import "./task.css"
import {FaCheck} from "@react-icons/all-files/fa/FaCheck";
import Countdown from "react-countdown";
import {FaArrowUp} from "@react-icons/all-files/fa/FaArrowUp";
import {FaTrash} from "@react-icons/all-files/fa/FaTrash";
import {routes} from "../Sidebar/Sidebar";
import {removeTask, updateTask} from "../common/taskProvider";
import {useState} from "react";
import EditTaskModal from "./edit/EditTaskModal";

const Task = ({ task, filteredTasks, setFilteredTasks }) => {

    const [data, setData] = useState(task)

    const [isDeleting, setIsDeleting] = useState(false);
    const [isMovingUp, setIsMovingUp] = useState(false);

    const [isOpen, setIsOpen] = useState(false);

    const handleDeleteTask = () => {
        setIsDeleting(true);

        setTimeout(() => {
            setFilteredTasks(filteredTasks.filter(task => task.id !== data?.id));
            removeTask(data?.id);
        }, 500);
    };

    const handleSendUp = () => {
        setIsMovingUp(true)

        setTimeout(() => {
            data.type = data?.type === "to-do" ? "in-progress" : "done"

            updateTask(data)
            setFilteredTasks(filteredTasks.filter(task => task.id !== data?.id));

        }, 500);

    }

    return (
        <div
            className={`task-card ${isDeleting ? 'fade-out' : ''} ${isMovingUp ? 'fade-up' : ''}`}
            key={data?.id}
        >
            <div className="task-header">
                <div className="window-header">
                    {routes.find(item => item.to === '/'+ data?.type)?.icon}
                    <div
                        className="ml-1 underline cursor-pointer"
                        key={data?.id + 'title'}
                        onClick={() => setIsOpen(!isOpen)}
                    >
                        {data?.title}
                    </div>
                    <EditTaskModal
                        key={'modal-for-edit-' + data?.id}
                        isOpen={isOpen}
                        setIsOpen={setIsOpen}
                        id={data?.id}
                        data={data}
                        setData={setData}
                    />
                </div>
                <div className="window-header">
                    {data?.type !== "done" && <FaArrowUp className="cursor-pointer" onClick={() => handleSendUp()} />}
                    <FaTrash className="ml-1 cursor-pointer" onClick={() => handleDeleteTask()}/>
                </div>
            </div>
            <div className="task-card-text">{data?.content}</div>
            {data?.type !== "done" &&
                <Countdown
                    date={new Date(data?.dueDate)}
                    renderer={({ days, hours, minutes, seconds, completed }) => (
                        <div className={`countdown ${completed ? "bg-[#F08080FF]" : "bg-[#f8bd56]"}`}>
                            {completed ? <span>Task expired</span> : <span>{days}d {hours}h {minutes}m {seconds}s</span>}
                        </div>
                    )}
                />
            }
            {data?.type === "done" && <div className="green-banner"><FaCheck /></div>}
        </div>
    )
}

export default Task