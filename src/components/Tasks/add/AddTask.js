import {useState} from "react";
import {Button} from "@mui/material";
import {FaPlus} from "@react-icons/all-files/fa/FaPlus";
import AddTaskModal from "./AddTaskModal";
import "./addTask.css"

const AddTask = ({ filteredTasks, setFilteredTasks, ...props }) => {
    const [isOpen, setIsOpen] = useState(false)

    return (
        <div {...props}>
            <Button
                className="add-task-button"
                onClick={() => setIsOpen(!isOpen)}
            >
                <FaPlus className="mr-2"/>Add Task
            </Button>
            <AddTaskModal isOpen={isOpen} setIsOpen={setIsOpen} filteredTasks={filteredTasks} setFilteredTasks={setFilteredTasks} />
        </div>
    )
}

export default AddTask