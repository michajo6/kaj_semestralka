import {Button, FormControl, Modal, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import "./addTask.css"
import {DateTimePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {DemoContainer} from "@mui/x-date-pickers/internals/demo";
import {useState} from "react";
import moment from "moment"
import {AdapterMoment} from "@mui/x-date-pickers/AdapterMoment";
import {useLocation, useNavigate} from "react-router-dom";
import {addTask, getStorageTasks} from "../../common/taskProvider";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    background: "rgb(255,255,255)",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4,
};

const AddTaskModal = ({ isOpen, setIsOpen, filteredTasks, setFilteredTasks }) => {

    const [title, setTitle] = useState("")
    const [content, setContent] = useState("")
    const [dueDate, setDueDate] = useState("")

    const location = useLocation();

    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();

        const task = {
            title: title,
            content: content,
            dueDate: dueDate ? moment(dueDate).format('Y-MM-DD HH:mm:ss') : "",
            type: "to-do",
            id: 'temp-id-lmaoo'
        }

        addTask(task)

        setIsOpen(false)

        if (location.pathname.split("/")[1] === "to-do") {
            setFilteredTasks(filteredTasks.concat(task))
        } else {
            navigate("/to-do")
        }
    }

    return (
        <Modal
            key='add-task-modal'
            open={isOpen}
            onClose={() => setIsOpen(false)}
        >
            <Box
                sx={style}
            >
                <form onSubmit={handleSubmit} className="add-task-form">
                    <FormControl fullWidth>
                        <TextField id="title" label="Title" variant="outlined" onChange={e => setTitle(e.target.value)} required />

                        <TextField id="content" label="Content" variant="outlined" onChange={e => setContent(e.target.value)} multiline minRows={5} required />

                        <LocalizationProvider dateAdapter={AdapterMoment}>
                            <DemoContainer components={['DateTimePicker']}>
                                <DateTimePicker
                                    id="due-date"
                                    label="Due date"
                                    onChange={newDate => setDueDate(newDate)}
                                    slotProps={{
                                    textField: {
                                        required: true,
                                    },
                                }}/>
                            </DemoContainer>
                        </LocalizationProvider>

                        <Button type="submit" variant="contained" className="add-task-button">
                            Submit
                        </Button>
                    </FormControl>
                </form>
            </Box>
        </Modal>
    )
}

export default AddTaskModal