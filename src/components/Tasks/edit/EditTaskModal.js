import {Button, FormControl, Modal, TextField} from "@mui/material";
import Box from "@mui/material/Box";
import "../add/addTask.css"
import {DateTimePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {DemoContainer} from "@mui/x-date-pickers/internals/demo";
import {useEffect, useState} from "react";
import moment from "moment"
import {AdapterMoment} from "@mui/x-date-pickers/AdapterMoment";
import {useNavigate} from "react-router-dom";
import {getStorageTasks, updateTask} from "../../common/taskProvider";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    background: "rgb(255,255,255)",
    borderRadius: "10px",
    boxShadow: 24,
    p: 4,
};

const EditTaskModal = ({ data, setData, isOpen, setIsOpen, id }) => {

    const [title, setTitle] = useState(data?.title)
    const [content, setContent] = useState(data?.content)
    const [dueDate, setDueDate] = useState(moment(data?.dueDate))

    const handleSubmit = (event) => {
        event.preventDefault();

        let newData = data
        newData.id = id
        newData.title = title
        newData.content = content
        newData.dueDate = dueDate ? moment(dueDate).format('Y-MM-DD HH:mm:ss') : ""

        updateTask(newData)
        setData(newData)

        setIsOpen(false)
    }

    return (
        <Modal
            open={isOpen}
            onClose={() => setIsOpen(false)}
        >
            <Box
                sx={style}
            >
                <form onSubmit={handleSubmit} className="add-task-form">
                    <FormControl fullWidth>
                        <TextField id="title" label="Title" variant="outlined" onChange={e => setTitle(e.target.value)} required
                                   defaultValue={title}
                        />

                        <TextField id="content" label="Content" variant="outlined" onChange={e => setContent(e.target.value)} multiline minRows={5} required
                                   defaultValue={content}
                        />

                        <LocalizationProvider dateAdapter={AdapterMoment}>
                            <DemoContainer components={['DateTimePicker']}>
                                <DateTimePicker
                                    id="due-date"
                                    label="Due date"
                                    onChange={newDate => setDueDate(newDate)}
                                    slotProps={{
                                        textField: {
                                            required: true,
                                        },
                                    }}
                                    defaultValue={dueDate}
                                />
                            </DemoContainer>
                        </LocalizationProvider>

                        <Button type="submit" variant="contained" className="add-task-button">
                            Submit
                        </Button>
                    </FormControl>
                </form>
            </Box>
        </Modal>
    )
}

export default EditTaskModal