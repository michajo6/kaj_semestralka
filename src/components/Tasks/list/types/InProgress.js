import List from "../List";

const InProgress = () => {
    return (
        <List type={"in-progress"} typeFriendly={"IN PROGRESS"}/>
    );
}

export default InProgress