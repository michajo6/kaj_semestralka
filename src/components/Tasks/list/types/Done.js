import List from "../List";

const Done = () => {
    return (
        <List type={"done"} typeFriendly={"DONE"}/>
    );
}

export default Done