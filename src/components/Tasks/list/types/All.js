import "../list.css"
import List from "../List";

const All = () => {
        return (
            <List type={"all"} typeFriendly={"ALL"} />
        );
}

export default All