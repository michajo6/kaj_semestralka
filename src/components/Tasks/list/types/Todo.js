import List from "../List";

const Todo = () => {
    return (
        <List type={"to-do"} typeFriendly={"TO DO"}/>
    );
}

export default Todo