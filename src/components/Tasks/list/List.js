import Task from "../Task";
import { ReactSortable } from "react-sortablejs";
import React, {useEffect, useState} from "react";
import AddTask from "../add/AddTask";
import {routes} from "../../Sidebar/Sidebar";
import {getStorageTasks, updateTasks} from "../../common/taskProvider";

const List = ({ type, typeFriendly }) => {
    const [filteredTasks, setFilteredTasks] = useState(
        type === "all" ? getStorageTasks() : getStorageTasks().filter(task => task.type === type)
    )

    const handleSort = newTasks => {
        if (newTasks.length > 0) {
            updateTasks(newTasks, type);
            setFilteredTasks(newTasks)
        }
    }

    return (
        <>
            <div className="list-header">
                <div className='content-center'>{routes.find(item => item.to === '/'+type)?.icon}</div>
                <div className='ml-3'>{typeFriendly}</div>
            </div>
            {filteredTasks.length > 0 && (
                <ReactSortable
                    animation={150}
                    list={filteredTasks}
                    setList={handleSort}
                    className="task-list"
                    handle=".task-header"
                >
                    {filteredTasks.map(task => (
                        <Task
                            task={task}
                            key={task.id + 'task-outlier-box'}
                            filteredTasks={filteredTasks}
                            setFilteredTasks={setFilteredTasks}
                        />
                    ))}
                </ReactSortable>
            )}
            <AddTask className='add-task-button-container' key='add-task-container' filteredTasks={filteredTasks} setFilteredTasks={setFilteredTasks}/>
        </>
    );
};

export default List;
