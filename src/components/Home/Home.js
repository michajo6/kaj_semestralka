import "./home.css"
import {useIsMobile} from "../common/useIsMobile";
import image from "../../images/docu.png"

const Home = () => {
    return (
        <div>
            <div className="taskhub-header">Welcome to TaskHub</div>
            <img src={image} style={{ maxHeight: "700px" }} />
        </div>
    )
}

export default Home