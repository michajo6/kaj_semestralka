import { Routes, Route } from "react-router-dom";
import Home from "../components/Home/Home";
import Todo from "../components/Tasks/list/types/Todo";
import Done from "../components/Tasks/list/types/Done";
import All from "../components/Tasks/list/types/All";
import InProgress from "../components/Tasks/list/types/InProgress";

const Router = () => {
 return (
     <Routes>
         <Route path="/" element={<Home />} />
         <Route path="/all" element={<All />} />
         <Route path="/to-do" element={<Todo />} />
         <Route path="/in-progress" element={<InProgress />} />
         <Route path="/done" element={<Done />} />
     </Routes>
 )
}

export default Router